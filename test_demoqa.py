import time
from selenium import webdriver
from selenium.webdriver.common.by import By


def test_checkbox_selection():
    browser = webdriver.Chrome()

    try:
        browser.get("https://demoqa.com/")
        time.sleep(3)
        assert "DEMOQA" in browser.title, "Не удалось открыть сайт DEMOQA"

        elements_button = browser.find_element(By.XPATH, '//*[@id="app"]/div/div/div[2]/div/div[1]')
        elements_button.click()
        time.sleep(2)
        assert "elements" in browser.current_url, "Страница 'Elements' не открылась"

        checkbox_menu = browser.find_element(By.XPATH, '//*[@id="item-1"]')
        checkbox_menu.click()
        time.sleep(2)
        assert "checkbox" in browser.current_url, "Страница 'Check Box' не открылась"

        home_directory = browser.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/div[2]/div['
                                                        '2]/div/ol/li/span/button')
        home_directory.click()
        time.sleep(2)
        downloads_directory = browser.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/div[2]/div['
                                                             '2]/div/ol/li/ol/li[3]/span/button')
        downloads_directory.click()
        time.sleep(2)

        wordfile_checkbox = browser.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/div[2]/div[2]/div['
                                                           '1]/ol/li/ol/li[3]/ol/li[1]/span/label/span[2]')
        wordfile_checkbox.click()
        time.sleep(2)

        message_element = browser.find_element(By.ID, "result")
        message_text = message_element.text
        assert "wordFile" in message_text, "Чекбокс Word File.doc не был выбран правильно"

        print("message:'You have selected: wordFile'")
    finally:
        browser.quit()
